# -*- coding: utf-8 -*-
"""
    seila.cli

    This is the CLI interface for running the package.
"""
import logging
import os
import sys
import pathlib
from functools import partial
from http.server import HTTPServer, SimpleHTTPRequestHandler
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from flask import Flask
from seila import __version__, __description__, logger, content


def simple_http_server() -> int:
    """
    Runs a Simple HTTP Server using python's standard http module. This currently serves the `public` directory and its
    contents as is.

    Returns:
         int: status code from the server, probably always 0.
    """
    web_dir = os.path.join(os.path.dirname(__file__), '../../simple_public')
    server_addr = ('0.0.0.0', 8000)
    handler = partial(SimpleHTTPRequestHandler, directory=web_dir)

    print(f"Starting SimpleHTTPServer at http://{server_addr[0]}:{server_addr[1]}")
    try:
        server = HTTPServer(server_addr, handler)
    except OSError as e:
        print(f"Could not open server: {e}.")
        return 1

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass

    print("Stopping SimpleHTTPServer")
    server.server_close()
    return 0


def flask_dev_server() -> int:
    """
    Runs a Flask WSGI server in development mode.

    Returns:
        int: status code from the server, probably always 0.
    """
    server_addr = '0.0.0.0'
    server_port = 8000
    this_directory = pathlib.Path(__file__).parent.resolve()

    app = Flask(
        'seila',
        static_folder=this_directory / '..' / '..' / 'static',
        static_url_path='/static',
        template_folder=this_directory / '..' / '..' / 'templates'
    )
    app.register_blueprint(content.bp_main_views)
    app.register_blueprint(content.bp_content)

    print(f"Starting Flask WSGI server at http://{server_addr}:{server_port}")
    try:
        app.run(host=server_addr, port=server_port, debug=True, use_reloader=True)
    except KeyboardInterrupt:
        pass

    print("Stopping Flask WSGI server")
    return 0


def main() -> int:
    """
    Main method which is called by CLI.

    Returns:
        int: status code from the server, probably always 0.
    """
    parser = ArgumentParser(
        formatter_class=RawDescriptionHelpFormatter,
        description=(
            f"seila {__version__}{os.linesep}"
            f"{__description__}"
        )
    )

    parser.add_argument(
        '-d',
        '--debug',
        help='Set LOG_LEVEL to DEBUG.',
        dest="debug_mode",
        action='store_true'
    )

    parser.add_argument(
        '-q',
        '--quiet',
        help='Do not log at all.',
        dest="quiet_mode",
        action='store_true'
    )

    parser.add_argument(
        'server_type',
        choices=['simple', 'flask']
    )

    # parse the arguments, show in the screen if needed, etc
    parser = parser.parse_args()

    if parser.debug_mode:
        logger.setLevel("DEBUG")
        logging.getLogger().setLevel(os.getenv("LOG_LEVEL", "DEBUG"))
    else:
        # defaults to info
        logger.setLevel("INFO")
        logging.getLogger().setLevel(os.getenv("LOG_LEVEL", "INFO"))

    if parser.quiet_mode:
        logging.disable(logging.CRITICAL)

    # run
    if parser.server_type == 'simple':
        exit_code = int(simple_http_server())
    elif parser.server_type == 'flask':
        exit_code = int(flask_dev_server())
    else:
        exit_code = 255

    return exit_code


if __name__ == '__main__':
    sys.exit(main())
