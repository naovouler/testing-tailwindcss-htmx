# -*- coding: utf-8 -*-
"""
    seila.content

    Flask routes
"""
import pathlib
from flask import Blueprint, render_template, send_from_directory, abort


bp_main_views = Blueprint('main_views', 'bp_main_views')
bp_content = Blueprint('content', 'bp_content', url_prefix='/content')
this_directory = pathlib.Path(__file__).parent.resolve()
content_directory = this_directory / '..' / '..' / 'content'


@bp_main_views.get('/')
def index() -> str:
    """
    Shows the root of the page

    Returns:
        str: HTML with the content
    """
    return render_template('base.html', content='initial_content')


@bp_content.get('/<content>')
def show_content(content: str) -> str:
    """
    Shows the base page with the content

    Params:
        content (str): The name of the content to fetch.

    Returns:
        str: HTML with the content
    """
    file_path = pathlib.Path(content_directory / f"{content}.html")
    if file_path.exists():
        return render_template('base.html', content=content)
    else:
        abort(404)


@bp_content.get('/<content>/r')
def show_content_raw(content: str) -> (str, int):
    """
    Shows the content of a file

    Params:
        content (str): The name of the file under `content` to return, without the `.html` suffix.

    Returns:
        Tuple(str, int): HTML with the content and status code
    """
    return send_from_directory(content_directory, f"{content}.html")