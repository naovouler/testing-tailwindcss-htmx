# -*- coding: utf-8 -*-
import logging
import os
import pathlib

this_directory = pathlib.Path(__file__).parent.resolve()

__author__ = 'Eitch'
__version__ = (this_directory / '..' / '..' / 'VERSION').read_text(encoding='utf-8')
__description__ = 'Description: Numsei!'

# logging setup
if not logging.getLogger().hasHandlers():
    logging.basicConfig(
        format='[%(asctime)s] [%(levelname)s] %(message)s'
    )
logging.getLogger().setLevel(os.getenv("LOG_LEVEL", "FATAL"))
logger = logging.getLogger(__name__)
