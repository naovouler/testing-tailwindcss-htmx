# -*- coding: utf-8 -*-
"""
    # seila setup script

    Setup script for packaging and installing seila
"""
import pathlib
from setuptools import setup, find_packages

this_directory = pathlib.Path(__file__).parent.resolve()
long_description = (this_directory / 'README.md').read_text(encoding='utf-8')
app_version = (this_directory / 'VERSION').read_text(encoding='utf-8')

setup(
    name="seila",
    version=app_version,
    description='Description: Numsei!',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Eitch',
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    python_requires=">=3.8, <4",
    install_requires=[
        'Flask==3.0.0',
    ],
    entry_points={
        'console_scripts': [
            'seila-cli=seila.cli:main'
        ],
    },
)
