// navigation icon on smaller displays
// -----------------------------------
document.getElementById("nav-icon").addEventListener('click',
  () => document.getElementById("nav-menu").classList.toggle("hidden")
);

// dark-mode
// ---------
// source: https://www.freecodecamp.org/news/how-to-build-a-dark-mode-switcher-with-tailwind-css-and-flowbite/
var themeToggleBtn = document.getElementById('theme-toggle');
var themeToggleDarkIcon = document.getElementById('theme-toggle-dark-icon');
var themeToggleLightIcon = document.getElementById('theme-toggle-light-icon');

if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
  document.documentElement.classList.add('dark');
  themeToggleLightIcon.classList.toggle('hidden');
  localStorage.theme = 'dark';
} else {
  document.documentElement.classList.remove('dark')
  themeToggleDarkIcon.classList.toggle('hidden');
  localStorage.theme = 'light';
}

themeToggleBtn.addEventListener('click', function() {
    themeToggleDarkIcon.classList.toggle('hidden');
    themeToggleLightIcon.classList.toggle('hidden');

    // clicked while on dark-mode - set to light
    if (themeToggleLightIcon.classList.contains('hidden')) {
        document.documentElement.classList.remove('dark');
        localStorage.theme = 'light';
    }

    // clicked while on light-mode - set to dark
    if (themeToggleDarkIcon.classList.contains('hidden')) {
        document.documentElement.classList.add('dark');
        localStorage.theme = 'dark';
    }
});